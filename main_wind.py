#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import (QWidget, QLabel,QPushButton, QApplication,
    QLineEdit, QApplication, QMainWindow, QAction, qApp)
from PyQt5.QtGui import QPixmap, QColor, QIcon
from PyQt5.QtCore import QCoreApplication, QTimer, QSize, QThread
import PyQt5
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder
from pymodbus.client.sync import ModbusSerialClient

import setting_wind
import copy
import  subprocess
import socket
import struct
import pickle
import time


# import read_data
# --->>> на дисплей
# <<<--- на контроллер

connect_status_glob = 0


class Window_Ui(QMainWindow):

    def __init__(self):
        super().__init__()

        self.initUI()



        # self.connect_modbus()


        # print(sys.executable)
        # subprocess.Popen([sys.executable, 'read_data.py', 'argzzz1', 'argzzz2'])

        # транспортер выкидной
        # --->>>
        # вкл/выкл
        self.conveyor_on_off_1 = 0
        # аварийное отключение по токовому реле
        self.conveyor_current_relay_1 = 2

        # транспортер подающий
        # --->>>
        # вкл/выкл
        self.conveyor_on_off_2 = 0
        # аварийное отключение по токовому реле
        self.conveyor_current_relay_2 = 2


        # молотковая дробилка
        # --->>>
        # вкл/выкл
        self.hammer_on_off = 0
        # авария по перегреву
        self.hammer_crash_overload = 2
        # скорость вращения
        self.hammer_speed = 0
        # сработка на отключение транспортера
        self.hammer_conveyor_off = 0
        # <<<---
        # установка скорости мин (отключение транспортера)
        self.hammer_set_off_speed_min = 0
        # установка скорости мин (отключение транспортера)
        self.hammer_set_speed_on = 0

        # вал разрывной
        # --->>>
        # вращения прямое
        self.val_direction_1_p = 0

        # вращения обратное
        self.val_direction_1_o = 0


        # срабатывание токового реле
        self.val_current_relay_1 = 2

        # срабатывание по датчику скорости
        self.val_sensor_speed_1 = 2

        # перегрев электродвигателя
        self.overheating_motor_1 = 2

        # зажатие вкл (вала)
        self.val_clamping_on_1 = 0

        # скорость вращения
        self.val_speed_1 = 0


        # количество поданных реверсов
        self.val_quantity_revers_1 = 0




        # <<<---
        # установка отключения по скорости min
        self.val_set_off_speed_min_1 = 0

        # установка времени включения
        self.val_set_on_time_1 = 0

        # установка времени фильтрации(срабатывание)
        self.val_set_time_actuation_1 = 0

        # установка времени на отсутствие импульсов
        self.val_set_time_pulse_off_1 = 0



        # Настройки
        # <<<---
        # время вкл выкидного транспортера
        self.time_on_conveyor_1 = 0
        # время выкл выкидного транспортера
        self.time_off_conveyor_1 = 0
        # время выключения молотковой дробилки
        self.time_off_hammer = 0
        # предпусковой звонок
        self.pre_launch = 0
        # время прямого вращения
        self.time_direct_rotation = 0
        # время обратного вражения
        self.time_reverse_rotation = 0
        # пауза на остановку
        self.pause_to_stop = 0


        # переменные для анимации

        self.alfa1 = 0
        self.alfa2 = 0
        self.alfa3 = 0
        self.alfa4 = 0
        self.alfa5 = 0
        self.alfa6 = 0

        self.alfa_hammer = 0

        self.alfa_conveyor_val_1 = 0
        self.alfa_conveyor_val_2 = 0

        self.test_x = 50


        # запуск потока

        # self.Test_mp_instance = Test_mp(mainwindow=self)
        # self.Test_mp_instance.start()





    def initUI(self):


        self.fone = QLabel(self)
        self.fone.setPixmap(QPixmap('img/fone1.png'))
        self.fone.setGeometry(0, 0, 1280, 800)


        # объекты для анимации
        self.drob1 = QLabel(self)
        self.drob1.setPixmap(QPixmap('img/crash_1.png'))
        self.drob1.setGeometry(450, 400, 200, 100)

        self.drob2 = QLabel(self)
        self.drob2.setPixmap(QPixmap('img/crash_2.png'))
        self.drob2.setGeometry(1150, 210, 100, 300)

        self.drob3 = QLabel(self)
        self.drob3.setPixmap(QPixmap('img/conv1.png'))
        self.drob3.setGeometry(300, 640, 350, 200)

        self.drob4 = QLabel(self)
        self.drob4.setPixmap(QPixmap('img/conv2.png'))
        self.drob4.setGeometry(925, 660, 350, 200)



        self.tooth1 = QLabel(self)
        self.tooth1.setPixmap(QPixmap('img/зуб2/0.png'))
        self.tooth1.setGeometry(25, 100, 75, 75)


        self.tooth_clamping_1 = QLabel(self)
        self.tooth_clamping_1.setPixmap(QPixmap('img/clamping_tooth_1.png'))
        self.tooth_clamping_1.setGeometry(25, 100, 75, 75)

        self.tooth_carrent_1 = QLabel(self)
        self.tooth_carrent_1.setPixmap(QPixmap('img/carrent_0.png'))
        # self.tooth_carrent_1.setPixmap(QPixmap('img/carrent_1.png').scaled(50, 50))
        self.tooth_carrent_1.setGeometry(100, 100, 75, 75)
        # self.tooth_carrent_1.setToolTip('Срабатывание токового реле')


        self.tooth_speed_1 = QLabel(self)
        self.tooth_speed_1.setPixmap(QPixmap('img/speed_0.png'))
        self.tooth_speed_1.setGeometry(175, 100,  75, 75)

        self.tooth_temp_1 = QLabel(self)
        self.tooth_temp_1.setPixmap(QPixmap('img/temp_0.png'))
        self.tooth_temp_1.setGeometry(250, 100,  75, 75)



        self.lbl_tooth_speed_1 = QLabel(self)
        self.lbl_tooth_speed_1.setText("Скорость")
        self.lbl_tooth_speed_1.adjustSize()
        self.lbl_tooth_speed_1.move(325, 80)
        self.lbl_tooth_speed_2 = QLabel(self)
        self.lbl_tooth_speed_2.setText("вращения")
        self.lbl_tooth_speed_2.adjustSize()
        self.lbl_tooth_speed_2.move(325, 90)

        self.lbl_tooth_revers_1 = QLabel(self)
        self.lbl_tooth_revers_1.setText("Количество")
        self.lbl_tooth_revers_1.adjustSize()
        self.lbl_tooth_revers_1.move(400, 80)
        self.lbl_tooth_revers_2 = QLabel(self)
        self.lbl_tooth_revers_2.setText("реверсов")
        self.lbl_tooth_revers_2.adjustSize()
        self.lbl_tooth_revers_2.move(400, 90)



        self.qle_motor_speed_1 = QLineEdit(self)
        self.qle_motor_speed_1.resize(50, 25)
        self.qle_motor_speed_1.move(325, 125)
        self.qle_motor_speed_1.setReadOnly(1)

        self.qle_revers_1 = QLineEdit(self)
        self.qle_revers_1.resize(50, 25)
        self.qle_revers_1.move(400, 125)
        self.qle_revers_1.setReadOnly(1)






        # анимация молотковой дробилки
        self.hammer = QLabel(self)
        self.hammer.setPixmap(QPixmap('img/молотковое_колесо/0.png'))
        self.hammer.setGeometry(700, 100, 80, 80)

        # окна визуализации параметров

        self.lbl_hammer_speed_1 = QLabel(self)
        self.lbl_hammer_speed_1.setText("Скорость")
        self.lbl_hammer_speed_1.adjustSize()
        self.lbl_hammer_speed_1.move(875, 80)
        self.lbl_hammer_speed_2 = QLabel(self)
        self.lbl_hammer_speed_2.setText("вращения")
        self.lbl_hammer_speed_2.adjustSize()
        self.lbl_hammer_speed_2.move(875, 90)

        self.qle_motor_speed_hammer = QLineEdit(self)
        self.qle_motor_speed_hammer.resize(50, 25)
        self.qle_motor_speed_hammer.move(875, 125)
        self.qle_motor_speed_hammer.setReadOnly(1)

        self.hammer_carrent_over_load = QLabel(self)
        self.hammer_carrent_over_load.setPixmap(QPixmap('img/over_load_0.png'))
        self.hammer_carrent_over_load.setGeometry(800, 115, 50, 50)



        # объкт анимации вала транспортера 1
        self.conveyor_val_1 = QLabel(self)
        self.conveyor_val_1.setPixmap(QPixmap('img/вал_транспортера/0.png'))
        self.conveyor_val_1.setGeometry(37, 550, 75, 75)

        self.conveyor_val_carrent_1 = QLabel(self)
        self.conveyor_val_carrent_1.setPixmap(QPixmap('img/carrent_0.png'))
        self.conveyor_val_carrent_1.setGeometry(100, 550, 75, 75)

        # объкт анимации вала транспортера 2
        self.conveyor_val_2 = QLabel(self)
        self.conveyor_val_2.setPixmap(QPixmap('img/вал_транспортера/0.png'))
        self.conveyor_val_2.setGeometry(700, 550, 75, 75)


        self.conveyor_val_carrent_2 = QLabel(self)
        self.conveyor_val_carrent_2.setPixmap(QPixmap('img/carrent_0.png'))
        self.conveyor_val_carrent_2.setGeometry(765, 550, 75, 75)



        self.qbtn_1_m = QPushButton('', self)
        self.qbtn_1_m.clicked.connect(self.window_setting_open)
        self.qbtn_1_m.setIcon(QIcon('img/setting.png'))
        self.qbtn_1_m.setToolTip('Настройки')
        self.qbtn_1_m.setIconSize(QSize(45, 45))
        self.qbtn_1_m.resize(50, 50)
        self.qbtn_1_m.move(0, 0)

        self.setting = setting_wind.Window_setting()
        # self.setting.qle_to_2.setText("test")
        # self.setting.qle_from_1.setReadOnly(1)

        self.qbtn_serial = QPushButton('', self)
        self.qbtn_serial.clicked.connect(self.window_connect_com_port_open)
        self.qbtn_serial.setIcon(QIcon('img/connect_img.png'))
        self.qbtn_serial.setToolTip('Настройки COM порта')
        self.qbtn_serial.setIconSize(QSize(45, 45))
        self.qbtn_serial.resize(50, 50)
        self.qbtn_serial.move(50, 0)

        self.setting_com_port = setting_wind.Window_com_port()


        self.qbtn_2_m = QPushButton('', self)
        self.qbtn_2_m.clicked.connect(self.window_setting_tooth_open)
        self.qbtn_2_m.setIcon(QIcon('img/setting.png'))
        self.qbtn_2_m.setToolTip('Настройка параметров разрывной дробилки')
        self.qbtn_2_m.setIconSize(QSize(45, 45))
        self.qbtn_2_m.resize(50, 50)
        self.qbtn_2_m.move(590, 0)

        self.setting_tooth = setting_wind.Window_tooth_setting()


        self.qbtn_3_m = QPushButton('', self)
        self.qbtn_3_m.clicked.connect(self.window_setting_hammer_open)
        self.qbtn_3_m.setIcon(QIcon('img/setting.png'))
        self.qbtn_3_m.setToolTip('Настройка параметров молотковой дробилки')
        self.qbtn_3_m.setIconSize(QSize(45, 45))
        self.qbtn_3_m.resize(50, 50)
        self.qbtn_3_m.move(1230, 0)

        self.setting_hammer = setting_wind.Window_setting_hammer()

        self.setGeometry(100, 100, 1280, 800)
        self.setWindowTitle('Modbus')
        self.show()

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.animation)
        self.timer.start(100)

        # настроека таймера
        self.timer2 = QTimer(self)
        # self.timer2.timeout.connect(self.read_modbus)
        self.timer2.timeout.connect(self.put_data)
        self.timer2.start(100)

    def animation_tooth(self, p, o, tooth, alfa):

        if(p and not o):
            if(alfa >= 350):
                alfa = 0
            alfa = alfa + 10
        elif(not p and o):
            if(alfa <= 0):
                alfa = 350
            alfa = alfa - 10
        elif(not p and not o):
            alfa = alfa

        name2 = 'img/зуб2/' + str(alfa) + '.png'
        tooth.setPixmap(QPixmap(name2))
        # возвращаем alfa для обновления переменной после выполнения функци
        return alfa

    def animation_tooth2(self, p, o, tooth, alfa):

        if(p and not o):
            if(alfa >= 350):
                alfa = 0
            alfa = alfa + 10
        elif(not p and o):
            if(alfa <= 0):
                alfa = 350
            alfa = alfa - 10
        elif(not p and not o):
            alfa = alfa

        name2 = 'img/зуб/' + str(alfa) + '.png'
        tooth.setPixmap(QPixmap(name2))
        # возвращаем alfa для обновления переменной после выполнения функци
        return alfa

    def animation_hammer(self, p, o, hammer, alfa):

        if(p and not o):
            if(alfa >= 350):
                alfa = 0
            alfa = alfa + 10
        elif(not p and o):
            if(alfa <= 0):
                alfa = 350
            alfa = alfa - 10
        elif(not p and not o):
            alfa = alfa

        name2 = 'img/молотковое_колесо/' + str(alfa) + '.png'
        hammer.setPixmap(QPixmap(name2))
        # возвращаем alfa для обновления переменной после выполнения функци
        return alfa

    def animation_conveyor_val(self, p, o, hammer, alfa):

        if(p and not o):
            if(alfa >= 350):
                alfa = 0
            alfa = alfa + 10
        elif(not p and o):
            if(alfa <= 0):
                alfa = 350
            alfa = alfa - 10
        elif(not p and not o):
            alfa = alfa

        name2 = 'img/вал_транспортера/' + str(alfa) + '.png'
        hammer.setPixmap(QPixmap(name2))
        # возвращаем alfa для обновления переменной после выполнения функци
        return alfa

    def animation(self):


        # тестовая проверка вращения зубьев
        self.alfa1 = self.animation_tooth(self.val_direction_1_p, self.val_direction_1_o, self.tooth1, self.alfa1)


        self.alfa_hammer = self.animation_hammer(0, self.hammer_on_off, self.hammer, self.alfa_hammer)

        self.alfa_conveyor_val_1 = self.animation_conveyor_val(self.conveyor_on_off_1, 0, self.conveyor_val_1, self.alfa_conveyor_val_1)
        self.alfa_conveyor_val_2 = self.animation_conveyor_val(self.conveyor_on_off_2, 0, self.conveyor_val_2, self.alfa_conveyor_val_2)



        if (self.val_current_relay_1==1):
            self.tooth_carrent_1.setPixmap(QPixmap("img/carrent_2.png"))
        elif(self.val_current_relay_1 == 0) :
            self.tooth_carrent_1.setPixmap(QPixmap("img/carrent_1.png"))
# _______________________________________________________________________________________




        if (self.val_sensor_speed_1 == 1):
            self.tooth_speed_1.setPixmap(QPixmap("img/speed_2.png"))
        elif(self.val_sensor_speed_1 == 0):
            self.tooth_speed_1.setPixmap(QPixmap("img/speed_1.png"))




        if (self.overheating_motor_1 == 1):
            self.tooth_temp_1.setPixmap(QPixmap("img/temp_2.png"))
        elif (self.overheating_motor_1 == 0):
            self.tooth_temp_1.setPixmap(QPixmap("img/temp_1.png"))


        # отображене зажатия
        if (self.val_clamping_on_1 == 1):
            self.tooth_clamping_1.setPixmap(QPixmap("img/clamping_tooth_2.png"))
        elif (self.val_clamping_on_1 == 0):
            self.tooth_clamping_1.setPixmap(QPixmap("img/clamping_tooth_1.png"))



        # сработка токового реле конвеера 1 2
        if (self.conveyor_current_relay_1 == 1):
            self.conveyor_val_carrent_1.setPixmap(QPixmap("img/carrent_2.png"))
        elif (self.conveyor_current_relay_1 == 0):
            self.conveyor_val_carrent_1.setPixmap(QPixmap("img/carrent_1.png"))

        if (self.conveyor_current_relay_2 == 1):
            self.conveyor_val_carrent_2.setPixmap(QPixmap("img/carrent_2.png"))
        elif (self.conveyor_current_relay_2 == 0):
            self.conveyor_val_carrent_2.setPixmap(QPixmap("img/carrent_1.png"))

        #  срабощтка на перегрузку молотковой дробилки
        if (self.hammer_crash_overload == 1):
            self.hammer_carrent_over_load.setPixmap(QPixmap("img/over_load_2.png"))
        elif (self.hammer_crash_overload == 0):
            self.hammer_carrent_over_load.setPixmap(QPixmap("img/over_load_1.png"))



    #  прорисовка запись данных на главном окне
    def put_data(self):

        # connect_status_glob = self.setting_com_port.connect_status

        if(self.setting_com_port.connect_status and self.setting_com_port.connect_status_flag):
            self.Test_mp_instance = Test_mp(mainwindow=self)
            self.Test_mp_instance.start()
            self.setting_com_port.connect_status_flag = 0


        # print("put_data")
        # обработка параметров и вывод индикации


        # скорость вращения молотковой дробилки
        self.qle_motor_speed_hammer.setText(str(round(self.hammer_speed,2)))

        # скорость вращения
        self.qle_motor_speed_1.setText(str(round(self.val_speed_1,2)))

        # количество поданных реверсов
        self.qle_revers_1.setText(str(round(self.val_quantity_revers_1,2)))




        # Установка по скорости min
        self.setting_tooth.qle_from_m_1_p_1.setText(str(round(self.val_set_off_speed_min_1,2)))
        # установка времени включения
        self.setting_tooth.qle_from_m_1_p_2.setText(str(round(self.val_set_on_time_1,2)))
        # установка времени фильтрации(срабатывание)
        self.setting_tooth.qle_from_m_1_p_3.setText(str(round(self.val_set_time_actuation_1,2)))
        # установка времени на отсутствие импульсов
        self.setting_tooth.qle_from_m_1_p_4.setText(str(round(self.val_set_time_pulse_off_1,2)))



        # установка скорости мин (отключение транспортера)
        self.setting_hammer.qle_from_1.setText(str(round(self.hammer_set_off_speed_min,2)))
        # установка скорости мин (отключение транспортера)
        self.setting_hammer.qle_from_2.setText(str(round(self.hammer_set_speed_on,2)))


        # отображение данных в окне настройки
        self.setting.qle_from_1.setText(str(round(self.time_on_conveyor_1,2)))
        self.setting.qle_from_2.setText(str(round(self.time_off_conveyor_1,2)))
        self.setting.qle_from_3.setText(str(round(self.time_off_hammer,2)))
        self.setting.qle_from_4.setText(str(round(self.pre_launch,2)))
        self.setting.qle_from_5.setText(str(round(self.time_direct_rotation,2)))
        self.setting.qle_from_6.setText(str(round(self.time_reverse_rotation,2)))
        self.setting.qle_from_7.setText(str(round(self.pause_to_stop,2)))


        if(not self.setting_com_port.connect_status):
            # self.client = self.setting_com_port.client
            self.setting_com_port.ConnectButton.setText("Подключиться")
            self.setting_com_port.ConnectButton.setEnabled(True)


    def window_setting_open(self):
        # self.setting = setting_wind.Window2()
        self.setting.show()
        # self.hide()

    def window_setting_tooth_open(self):
        # self.setting = setting_wind.Window2()
        self.setting_tooth.show()
        # self.hide()

    def window_setting_hammer_open(self):
        self.setting_hammer.show()


    def window_connect_com_port_open(self):
        # self.setting = setting_wind.Window2()
        self.setting_com_port.show()
        # self.hide()



    # def connect_modbus(self):
    #     self.client = ModbusSerialClient(method="rtu", port="COM15", stopbits=1, bytesize=8, parity="N", baudrate=115200, timeout=0.2)
    #     # connection = self.client.connect()
    #     self.connect_status = self.client.connect()
    #     print("Connect_status", self.connect_status)



    def read_modbus(self):


        if(self.setting_com_port.connect_status):

            self.client = self.setting_com_port.client
            # write
            if(self.setting_hammer.status_but == 1):
                print("but_setting_hammer")

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_hammer.hammer_set_off_speed_min)
                registers = builder.to_registers()
                self.client.write_registers(4, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_hammer.hammer_set_speed_on)
                registers = builder.to_registers()
                self.client.write_registers(6, registers, unit=2)

                self.setting_hammer.status_but = 0


            # self.setting_tooth
            if(self.setting_tooth.status_but == 1):
                print("but_setting_tooth")


                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_off_speed_min_1)
                registers = builder.to_registers()
                self.client.write_registers(36, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_off_speed_min_2)
                registers = builder.to_registers()
                self.client.write_registers(38, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_off_speed_min_3)
                registers = builder.to_registers()
                self.client.write_registers(40, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_off_speed_min_4)
                registers = builder.to_registers()
                self.client.write_registers(42, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_off_speed_min_5)
                registers = builder.to_registers()
                self.client.write_registers(44, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_off_speed_min_6)
                registers = builder.to_registers()
                self.client.write_registers(46, registers, unit=2)



                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_on_time_1)
                registers = builder.to_registers()
                self.client.write_registers(48, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_on_time_2)
                registers = builder.to_registers()
                self.client.write_registers(50, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_on_time_3)
                registers = builder.to_registers()
                self.client.write_registers(52, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_on_time_4)
                registers = builder.to_registers()
                self.client.write_registers(54, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_on_time_5)
                registers = builder.to_registers()
                self.client.write_registers(56, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_on_time_6)
                registers = builder.to_registers()
                self.client.write_registers(58, registers, unit=2)





                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_time_actuation_1)
                registers = builder.to_registers()
                self.client.write_registers(60, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_time_actuation_2)
                registers = builder.to_registers()
                self.client.write_registers(62, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_time_actuation_3)
                registers = builder.to_registers()
                self.client.write_registers(64, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_time_actuation_4)
                registers = builder.to_registers()
                self.client.write_registers(66, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_time_actuation_5)
                registers = builder.to_registers()
                self.client.write_registers(68, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_time_actuation_6)
                registers = builder.to_registers()
                self.client.write_registers(70, registers, unit=2)



                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_time_pulse_off_1)
                registers = builder.to_registers()
                self.client.write_registers(72, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_time_pulse_off_2)
                registers = builder.to_registers()
                self.client.write_registers(74, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_time_pulse_off_3)
                registers = builder.to_registers()
                self.client.write_registers(76, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_time_pulse_off_4)
                registers = builder.to_registers()
                self.client.write_registers(78, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_time_pulse_off_5)
                registers = builder.to_registers()
                self.client.write_registers(80, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_time_pulse_off_6)
                registers = builder.to_registers()
                self.client.write_registers(82, registers, unit=2)

                self.setting_tooth.status_but = 0

            if(self.setting.status_but == 1):
                print("but_setting")
                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting.time_on_conveyor_1)
                registers = builder.to_registers()
                self.client.write_registers(84, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting.time_off_conveyor_1)
                registers = builder.to_registers()
                self.client.write_registers(86, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting.time_off_hammer)
                registers = builder.to_registers()
                self.client.write_registers(88, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting.pre_launch)
                registers = builder.to_registers()
                self.client.write_registers(90, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting.time_direct_rotation)
                registers = builder.to_registers()
                self.client.write_registers(92, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting.time_reverse_rotation)
                registers = builder.to_registers()
                self.client.write_registers(94, registers, unit=2)

                builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting.pause_to_stop)
                registers = builder.to_registers()
                self.client.write_registers(96, registers, unit=2)

                self.setting.status_but = 0

            s_time = time.time()

            # чтение 2х 8-bits  (reg №0)
            try:
                result = self.client.read_input_registers(0, 1,  unit=2)
                reg_0 = self.decode_2_8bit(result)
                reg_0 = copy.deepcopy(list(reversed(reg_0)))
                # print(reg_0)
                self.conveyor_on_off_1 = reg_0[0]
                self.conveyor_current_relay_1 = reg_0[1]
                self.conveyor_on_off_2 = reg_0[2]
                self.conveyor_current_relay_2 = reg_0[3]

                self.hammer_on_off = reg_0[8]
                self.hammer_crash_overload = reg_0[9]
                self.hammer_conveyor_off = reg_0[10]
                # reg_0
            except AttributeError:
                print("ModbusIOException object has no attribute registers1")

            # чтение float (reg №2-3)
            try:
                result = self.client.read_holding_registers(2, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_2_3 = decoder.decode_32bit_float()
                # print(reg_2_3)
                self.hammer_speed = reg_2_3
            except AttributeError:
                print("ModbusIOException object has no attribute registers2")


            # чтение float (reg_4_5)
            try:
                result = self.client.read_holding_registers(4, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_4_5 = decoder.decode_32bit_float()
                # print(reg_4_5)
                self.hammer_set_off_speed_min = reg_4_5
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_6_7)
            try:
                result = self.client.read_holding_registers(6, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_6_7 = decoder.decode_32bit_float()
                # print(reg_6_7)
                self.hammer_set_speed_on = reg_6_7
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение 2х 8-bits  (reg_8)
            # вращение прямое; обратное
            try:
                result = self.client.read_input_registers(8, 1,  unit=2)
                reg_8 = self.decode_2_8bit(result)
                reg_8 = copy.deepcopy(list(reversed(reg_8)))
                # print(reg_8)
                # вращения прямое
                self.val_direction_1_p = reg_8[0]
                self.val_direction_2_p = reg_8[1]
                self.val_direction_3_p = reg_8[2]
                self.val_direction_4_p = reg_8[3]
                self.val_direction_5_p = reg_8[4]
                self.val_direction_6_p = reg_8[5]

                # вращения обратное
                self.val_direction_1_o = reg_8[8]
                self.val_direction_2_o = reg_8[9]
                self.val_direction_3_o = reg_8[10]
                self.val_direction_4_o = reg_8[11]
                self.val_direction_5_o = reg_8[12]
                self.val_direction_6_o = reg_8[13]
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение 2х 8-bits  (reg_9)
            # рабатывание токового реле, датчику скорости
            try:
                result = self.client.read_input_registers(9, 1,  unit=2)
                reg_9 = self.decode_2_8bit(result)
                reg_9 = copy.deepcopy(list(reversed(reg_9)))
                # print(reg_9)
                # срабатывание токового реле
                self.val_current_relay_1 = reg_9[0]
                self.val_current_relay_2 = reg_9[1]
                self.val_current_relay_3 = reg_9[2]
                self.val_current_relay_4 = reg_9[3]
                self.val_current_relay_5 = reg_9[4]
                self.val_current_relay_6 = reg_9[5]

                # срабатывание по датчику скорости
                self.val_sensor_speed_1 = reg_9[8]
                self.val_sensor_speed_2 = reg_9[9]
                self.val_sensor_speed_3 = reg_9[10]
                self.val_sensor_speed_4 = reg_9[11]
                self.val_sensor_speed_5 = reg_9[12]
                self.val_sensor_speed_6 = reg_9[13]
            except AttributeError:
                print("ModbusIOException object has no attribute registers")


            # чтение 2х 8-bits  (reg_10)
            # перегрев электродвигателя; зажатие вала
            try:
                result = self.client.read_input_registers(10, 1,  unit=2)
                reg_10 = self.decode_2_8bit(result)
                reg_10 = copy.deepcopy(list(reversed(reg_10)))
                # print(reg_10)
                # перегрев электродвигателя
                self.overheating_motor_1 = reg_10[0]
                self.overheating_motor_2 = reg_10[1]
                self.overheating_motor_3 = reg_10[2]
                self.overheating_motor_4 = reg_10[3]
                self.overheating_motor_5 = reg_10[4]
                self.overheating_motor_6 = reg_10[5]

                # зажатие вкл (вала)
                self.val_clamping_on_1 = reg_10[8]
                self.val_clamping_on_2 = reg_10[9]
                self.val_clamping_on_3 = reg_10[10]
                self.val_clamping_on_4 = reg_10[11]
                self.val_clamping_on_5 = reg_10[12]
                self.val_clamping_on_6 = reg_10[13]
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # скорость вала
            # чтение float (reg_162_13)
            try:
                result = self.client.read_holding_registers(12, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_12_13 = decoder.decode_32bit_float()
                # print(reg_12_13)
                self.val_speed_1 = reg_12_13
            except AttributeError:
                print("ModbusIOException object has no attribute registers")


            # чтение float (reg_14_15)
            try:
                result = self.client.read_holding_registers(14, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_14_15 = decoder.decode_32bit_float()
                # print(reg_14_15)
                self.val_speed_2 = reg_14_15
            except AttributeError:
                print("ModbusIOException object has no attribute registers")


            # чтение float (reg_16_17)
            try:
                result = self.client.read_holding_registers(16, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_16_17 = decoder.decode_32bit_float()
                # print(reg_16_17)
                self.val_speed_3 = reg_16_17
            except AttributeError:
                print("ModbusIOException object has no attribute registers")


            # чтение float (reg_18_19)
            try:
                result = self.client.read_holding_registers(18, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_18_19 = decoder.decode_32bit_float()
                # print(reg_18_19)
                self.val_speed_4 = reg_18_19
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_20_21)
            try:
                result = self.client.read_holding_registers(20, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_20_21 = decoder.decode_32bit_float()
                # print(reg_20_21)
                self.val_speed_5 = reg_20_21
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_22_23)
            try:
                result = self.client.read_holding_registers(22, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_22_23 = decoder.decode_32bit_float()
                # print(reg_22_23)
                self.val_speed_6 = reg_22_23
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # количество поданых реверсов
            # чтение float (reg_24_25)
            try:
                result = self.client.read_holding_registers(24, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_24_25 = decoder.decode_32bit_float()
                # print(reg_24_25)
                self.val_quantity_revers_1 = reg_24_25
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
            # чтение float (reg_26_27)
            try:
                result = self.client.read_holding_registers(26, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_26_27 = decoder.decode_32bit_float()
                # print(reg_26_27)
                self.val_quantity_revers_2 = reg_26_27
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_28_29)
            try:
                result = self.client.read_holding_registers(28, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_28_29 = decoder.decode_32bit_float()
                # print(reg_28_29)
                self.val_quantity_revers_3 = reg_28_29
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_30_31)
            try:
                result = self.client.read_holding_registers(30, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_30_31 = decoder.decode_32bit_float()
                # print(reg_30_31)
                self.val_quantity_revers_4 = reg_30_31
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_32_33)
            try:
                result = self.client.read_holding_registers(32, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_32_33 = decoder.decode_32bit_float()
                # print(reg_32_33)
                self.val_quantity_revers_5 = reg_32_33
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_34_35)
            try:
                result = self.client.read_holding_registers(34, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_34_35 = decoder.decode_32bit_float()
                # print(reg_34_35)
                self.val_quantity_revers_6 = reg_34_35
            except AttributeError:
                print("ModbusIOException object has no attribute registers")


            # установка отключения по скорости min
            # чтение float (reg_36_37)
            try:
                result = self.client.read_holding_registers(36, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_36_37 = decoder.decode_32bit_float()
                # print(reg_36_37)
                self.val_set_off_speed_min_1 = reg_36_37
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_38_39)
            try:
                result = self.client.read_holding_registers(38, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_38_39 = decoder.decode_32bit_float()
                # print(reg_38_39)
                self.val_set_off_speed_min_2 = reg_38_39
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_40_41)
            try:
                result = self.client.read_holding_registers(40, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_40_41 = decoder.decode_32bit_float()
                # print(reg_40_41)
                self.val_set_off_speed_min_3 = reg_40_41
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_42_43)
            try:
                result = self.client.read_holding_registers(42, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_42_43 = decoder.decode_32bit_float()
                # print(reg_42_43)
                self.val_set_off_speed_min_4 = reg_42_43
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_44_45)
            try:
                result = self.client.read_holding_registers(44, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_44_45 = decoder.decode_32bit_float()
                # print(reg_44_45)
                self.val_set_off_speed_min_5 = reg_44_45
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_46_47)
            try:
                result = self.client.read_holding_registers(46, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_46_47 = decoder.decode_32bit_float()
                # print(reg_46_47)
                self.val_set_off_speed_min_6 = reg_46_47
            except AttributeError:
                print("ModbusIOException object has no attribute registers")


            # установка времени включенияя
            # чтение float (reg_48_49)
            try:
                result = self.client.read_holding_registers(48, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_48_49 = decoder.decode_32bit_float()
                # print(reg_48_49)
                self.val_set_on_time_1 = reg_48_49
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_50_51)
            try:
                result = self.client.read_holding_registers(50, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_50_51 = decoder.decode_32bit_float()
                # print(reg_50_51)
                self.val_set_on_time_2 = reg_50_51
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_52_53)
            try:
                result = self.client.read_holding_registers(52, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_52_53 = decoder.decode_32bit_float()
                # print(reg_52_53)
                self.val_set_on_time_3 = reg_52_53
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_54_55)
            try:
                result = self.client.read_holding_registers(54, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_54_55 = decoder.decode_32bit_float()
                # print(reg_54_55)
                self.val_set_on_time_4 = reg_54_55
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_56_57)
            try:
                result = self.client.read_holding_registers(56, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_56_57 = decoder.decode_32bit_float()
                # print(reg_56_57)
                self.val_set_on_time_5 = reg_56_57
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
            # чтение float (reg_58_59)
            try:
                result = self.client.read_holding_registers(58, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_58_59 = decoder.decode_32bit_float()
                # print(reg_58_59)
                self.val_set_on_time_6 = reg_58_59
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # установка времени фильтрации (срабатывания)
            # чтение float (reg_60_61)
            try:
                result = self.client.read_holding_registers(60, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_60_61 = decoder.decode_32bit_float()
                # print(reg_60_61)
                self.val_set_time_actuation_1 = reg_60_61
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_62_63)
            try:
                result = self.client.read_holding_registers(62, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_62_63 = decoder.decode_32bit_float()
                # print(reg_62_63)
                self.val_set_time_actuation_2 = reg_62_63
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_64_65)
            try:
                result = self.client.read_holding_registers(64, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_64_65 = decoder.decode_32bit_float()
                # print(reg_64_65)
                self.val_set_time_actuation_3 = reg_64_65
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_66_67)
            try:
                result = self.client.read_holding_registers(66, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_66_67 = decoder.decode_32bit_float()
                # print(reg_66_67)
                self.val_set_time_actuation_4 = reg_66_67
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_68_69)
            try:
                result = self.client.read_holding_registers(68, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_68_69 = decoder.decode_32bit_float()
                # print(reg_68_69)
                self.val_set_time_actuation_5 = reg_68_69
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_70_71)
            try:
                result = self.client.read_holding_registers(70, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_70_71 = decoder.decode_32bit_float()
                # print(reg_70_71)
                self.val_set_time_actuation_6 = reg_70_71
            except AttributeError:
                print("ModbusIOException object has no attribute registers")


            # установка времени на отсутствие импульсов
            # чтение float (reg_72_73)
            try:
                result = self.client.read_holding_registers(72, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_72_73 = decoder.decode_32bit_float()
                # print(reg_72_73)
                self.val_set_time_pulse_off_1 = reg_72_73
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_74_75)
            try:
                result = self.client.read_holding_registers(74, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_74_75 = decoder.decode_32bit_float()
                # print(reg_74_75)
                self.val_set_time_pulse_off_2 = reg_74_75
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_76_77)
            try:
                result = self.client.read_holding_registers(76, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_76_77 = decoder.decode_32bit_float()
                # print(reg_76_77)
                self.val_set_time_pulse_off_3 = reg_76_77
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_78_79)
            try:
                result = self.client.read_holding_registers(78, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_78_79 = decoder.decode_32bit_float()
                # print(reg_78_79)
                self.val_set_time_pulse_off_4 = reg_78_79
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_80_81)
            try:
                result = self.client.read_holding_registers(80, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_80_81 = decoder.decode_32bit_float()
                # print(reg_80_81)
                self.val_set_time_pulse_off_5 = reg_80_81
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # чтение float (reg_82_83)
            try:
                result = self.client.read_holding_registers(82, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_82_83 = decoder.decode_32bit_float()
                # print(reg_82_83)
                self.val_set_time_pulse_off_6 = reg_82_83
            except AttributeError:
                print("ModbusIOException object has no attribute registers")


            # настройки
            # время вкл выкидного транспортера
            # чтение float (reg_84_85)
            try:
                result = self.client.read_holding_registers(84, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_84_85 = decoder.decode_32bit_float()
                # print(reg_84_85)
                self.time_on_conveyor_1 = reg_84_85
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # время выкл выкидного транспортера
            # чтение float (reg_86_87)
            try:
                result = self.client.read_holding_registers(86, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_86_87 = decoder.decode_32bit_float()
                # print(reg_86_87)
                self.time_off_conveyor_1 = reg_86_87
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
            # время выключения молотковой дробилки
            # чтение float (reg_88_89)
            try:
                result = self.client.read_holding_registers(88, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_88_89 = decoder.decode_32bit_float()
                # print(reg_88_89)
                self.time_off_hammer = reg_88_89
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
            # предпусковой звонок
            # чтение float (reg_90_91)
            try:
                result = self.client.read_holding_registers(90, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_90_91 = decoder.decode_32bit_float()
                # print(reg_90_91)
                self.pre_launch = reg_90_91
            except AttributeError:
                print("ModbusIOException object has no attribute registers")

            # время прямого вращения
            # чтение float (reg_92_93)
            try:
                result = self.client.read_holding_registers(92, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_92_93 = decoder.decode_32bit_float()
                # print(reg_92_93)
                self.time_direct_rotation = reg_92_93
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
            # время обратного вражения
            # чтение float (reg_94_95)
            try:
                result = self.client.read_holding_registers(94, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_94_95 = decoder.decode_32bit_float()
                # print(reg_94_95)
                self.time_reverse_rotation = reg_94_95
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
            # пауза на остановку
            # чтение float (reg_96_97)
            try:
                result = self.client.read_holding_registers(96, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_96_97 = decoder.decode_32bit_float()
                # print(reg_96_97)
                self.pause_to_stop = reg_96_97
            except AttributeError:
                print("ModbusIOException object has no attribute registers")




            f_time = time.time()
            d_time = f_time - s_time
            # print(d_time)






    def decode_2_8bit(self, data):

        # print("-"*60)
        # print(data.registers)
        # print()

        line_registers = "{0:b}".format(data.registers[0])

        array_reg = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

        index = len(array_reg)-1
        size_line = len(line_registers)
        index_line = size_line - 1

        while(index_line >= 0):
            array_reg[index] = int(line_registers[index_line])
            index = index - 1
            index_line = index_line - 1

        # print(array_reg)
        return array_reg


class Test_mp(QThread):
    def __init__(self, mainwindow, parent=None):
        super().__init__()
        self.mainwindow = mainwindow
        self.value = 0


    def run(self):
        # value = self.mainwindow.prograsbas.value()
        time_1 = time.time()
        while 1:
            if(time.time() - time_1> 0.2):
                time_1 = time.time()
                self.value = self.value + 1
                # self.mainwindow.prograsbas.setValue(value)
                self.mainwindow.read_modbus()
                # print(self.value)
                # time.sleep(0.2)



if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Window_Ui()
    # menus = MenuDemo()
    # ex = Main_Window()
    sys.exit(app.exec_())
